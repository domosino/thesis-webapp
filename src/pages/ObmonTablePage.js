import React, { Fragment } from "react";
import DataRenderer from "../molecules/DataRenderer";
import ServiceSelector from "../molecules/ServiceSelector";

const ObmonTablePage = () => {
  return (
    <Fragment>
      <ServiceSelector />
      <DataRenderer />
    </Fragment>
  );
};

export default ObmonTablePage;
