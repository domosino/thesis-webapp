import React, { Fragment, useEffect } from "react";
import { useDispatch } from "react-redux";
import { onSalsaReset } from "../actions/salsa";
import SalsaPanels from "../organisms/SalsaPanels";
import ServiceSelector from "../molecules/ServiceSelector";

const SalsaTablePage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(onSalsaReset());
  }, [dispatch]);
  return (
    <Fragment>
      <ServiceSelector />
      <SalsaPanels />
    </Fragment>
  );
};

export default SalsaTablePage;
