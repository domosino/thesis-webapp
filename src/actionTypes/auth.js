export const AUTH_USER = "AUTH_USER";
export const AUTH_ERROR = "AUTH_ERROR";
export const LOADING_LOGIN = "LOADING_LOGIN";
export const SET_USER_DATA = "SET_USER_DATA";