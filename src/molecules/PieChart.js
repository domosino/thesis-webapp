import { makeStyles } from "@material-ui/core";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import React from "react";
import { useSelector } from "react-redux";
import { getAppInfo } from "../selectors/websocket";

const useStyle = makeStyles({
  wrapper: {
    width: "100%",
  },
});

const PieChart = () => {
  const classes = useStyle();
  const { services } = useSelector(getAppInfo);
  let uniqueNames = [...new Set(services.map((x) => x.name))];
  uniqueNames = uniqueNames.map((name) => ({
    name,
    y: services.filter((x) => x.name === name).length,
    sliced: false,
  }));

  const chartOptions = {
    credits: {
      enabled: false,
    },
    title: {
      text: "",
    },
    series: [
      {
        name: "Count",
        colorByPoint: true,
        data: uniqueNames,
      },
    ],
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: "pie",
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: "pointer",
        dataLabels: {
          enabled: true,
          format: "<b>{point.name}</b>: {point.percentage:.1f} %",
        },
      },
    },
  };
  return (
    <div className={classes.wrapper}>
      <HighchartsReact highcharts={Highcharts} options={chartOptions} />
    </div>
  );
};

export default PieChart;
