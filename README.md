# Thesis webapp

## Maintainers

- [Dominik Matis](https://gitlab.com/domosino)

## Setup & run

> Install dependencies (creates `node_modules` dir)

- `npm ci`
  > Run dev server on port `3000`
- `npm start`

## Abilities

- login using LDAP server
