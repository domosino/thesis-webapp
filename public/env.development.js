window.env = {
  // This option can be retrieved in "src/index.js" with "window.env.API_URL".
  // API_URL: "localhost:8442/api",
  // API_WS_URL: "ws://localhost:8442/api",
  // HTTP_PROTOCOL: "http://",

  API_URL: "salsa-hlit.jinr.ru/api",
  API_WS_URL: "wss://salsa-hlit.jinr.ru/api",
  HTTP_PROTOCOL: "https://",
  // API_FORCE_WS_PROTOCOL: "ws://",
  HTTP_REQUEST_TIMEOUT: "30000",

  // REACT_APP_MAPS_KEY: "AIzaSyCUiOwL1ra5dkw2YHQIB7hVTfD0JmknYe4",
  REACT_APP_MAPS_KEY: " ",
};
