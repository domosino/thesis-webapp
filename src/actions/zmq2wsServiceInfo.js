import {
  GET_ZMQ2SERVICE_NAME,
  GET_ZMQ2SERVICE_SUB,
  GET_ZMQ2SERVICE_SRC,
  GET_ZMQ2SERVICE_DATA,
  GET_ZMQ2SERVICE_ISSUBSCRIBED
} from "../actionTypes/zmq2wsServiceInfo";

export const getNameService = name => ({
  type: GET_ZMQ2SERVICE_NAME,
  name
});

export const getSubService = sub => ({
  type: GET_ZMQ2SERVICE_SUB,
  sub
});

export const getSrc = src => ({
  type: GET_ZMQ2SERVICE_SRC,
  src
});

export const getData = data => ({
  type: GET_ZMQ2SERVICE_DATA,
  data
});

export const getIsSubscribed = isSub => ({
  type: GET_ZMQ2SERVICE_ISSUBSCRIBED,
  isSub
});
