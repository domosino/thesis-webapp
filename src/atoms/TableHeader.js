import { makeStyles, TableHead, TableRow } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import TableCell from "./TableCell";
import { getServiceData } from "../selectors/zmq2wsServiceInfo";

const tableHeaders = [
  { id: 1, name: "", colSpan: 4 },
  { id: 2, name: "CPU", colSpan: 5 },
  { id: 3, name: "Memory", colSpan: 3 },
  { id: 6, name: "Disks", colSpan: 2 },
  { id: 4, name: "Network", colSpan: 2 },
];

const useStyle = makeStyles({
  cell: {
    textAlign: "center",
    borderLeft: "1px solid #ccc",
    width: "50px",
    fontSize: "13px",
  },
});

const TableHeader = () => {
  const classes = useStyle();
  const serviceData = useSelector(getServiceData);

  const renderHeaders = () => {
    let nodeWithGPU = false;
    serviceData.forEach((node) => {
      if (node.gpu) {
        nodeWithGPU = true;
      }
    });
    if (nodeWithGPU) {
      return [
        ...tableHeaders,
        {
          id: 5,
          name: "GPU",
          colSpan: 4,
        },
      ];
    }
    return tableHeaders;
  };

  const headers = renderHeaders();
  return (
    <TableHead>
      <TableRow>
        {headers.map((header) => {
          return (
            <TableCell
              key={header.id}
              colSpan={header.colSpan}
              className={classes.cell}
            >
              {header.name}
            </TableCell>
          );
        })}
      </TableRow>
    </TableHead>
  );
};

export default TableHeader;
