import { WRITE_DATA_WS } from "../actionTypes/websocket";

// export const receiveAppInfoSuccess = (data) => ({
//     type: RECEIVE_APP_INFO_SUCCESS,
//     payload: data
// });

export const writeToSocket = data => ({
  type: WRITE_DATA_WS,
  payload: {
    message: data
  },
  meta: { socket: true }
});
