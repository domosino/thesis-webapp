import { v4 } from "uuid";

const totalTaskList = (tasks, classes) => [
  {
    id: v4(),
    className: classes.total,
    colSpan: 1,
    children: `Total`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: tasks.reduce((sum, task) => sum + task.total, 0)
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: tasks.reduce((sum, task) => sum + task.pending, 0)
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: tasks.reduce((sum, task) => sum + task.running, 0)
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: tasks.reduce((sum, task) => sum + task.finished, 0)
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: tasks.reduce((sum, task) => sum + task.failed, 0)
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `${Math.round(
      (tasks.reduce((sum, task) => sum + task.finished + task.failed, 0) /
        tasks.reduce((sum, task) => sum + task.total, 0)) *
        100
    )} %`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  }
];

export default totalTaskList;
