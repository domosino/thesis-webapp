import {
  AppBar,
  Icon,
  IconButton,
  makeStyles,
  Toolbar,
  Typography,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { removeToken } from "../actions/auth";
import { writeToSocket } from "../actions/websocket";
import { getAuthenticatedToken, getUserData } from "../selectors/auth";
import {
  getAppInfo,
  getWebsocketId,
  getWsConnected,
} from "../selectors/websocket";
import Sidebar from "./Sidebar";

const useStyle = makeStyles({
  wrapper: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
    color: "#fff",
  },
  users: {
    display: "flex",
    alignItems: "center",
  },
  iconGroup: (wsConnected) => ({
    marginRight: "16px",
    marginTop: "8px",
    color: wsConnected ? "#fff" : "red",
  }),
  numberClients: {
    marginTop: "2px",
  },
  link: {
    textDecoration: "none",
    color: "inherit",
  },
  logout: {
    cursor: "pointer",
    marginLeft: "16px",
  },
});

const Navbar = () => {
  const [openDrawer, setOpenDrawer] = useState(false);
  const history = useHistory();
  const auth = useSelector(getAuthenticatedToken);
  const appInfo = useSelector(getAppInfo);
  const websocketId = useSelector(getWebsocketId);
  const userData = useSelector(getUserData);
  const wsConnected = useSelector(getWsConnected);
  const classes = useStyle(wsConnected);
  const dispatch = useDispatch();

  const logout = () => {
    localStorage.removeItem("token");
    dispatch(removeToken());
    history.push("/");
  };

  useEffect(() => {
    if (userData && auth && websocketId !== "") {
      const { profile } = userData;
      const { uid, uidNumber } = profile;
      dispatch(
        writeToSocket({
          type: "updateinfo",
          username: uid,
          uid: uidNumber,
        })
      );
    }
  }, [websocketId, auth, dispatch, userData]);

  const renderRightNavbarMenu = () => {
    if (auth) {
      return (
        <React.Fragment>
          <Typography>
            <Link className={classes.link} to="/profile">
              <Icon>account_circle</Icon>
            </Link>
          </Typography>
          <Typography
            className={classes.logout}
            variant="subtitle1"
            color="inherit"
            onClick={logout}
          >
            <Icon>exit_to_app</Icon>
          </Typography>
        </React.Fragment>
      );
    } else {
      return (
        <Typography variant="subtitle1" color="inherit">
          <Link to="/auth" className={classes.link}>
            Login
          </Link>
        </Typography>
      );
    }
  };

  return (
    <div className={classes.wrapper}>
      <Sidebar open={openDrawer} toggle={(close) => setOpenDrawer(close)} />
      <AppBar position="static">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="Menu"
            onClick={() => setOpenDrawer(true)}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="subtitle1"
            color="inherit"
            className={classes.flex}
          >
            <div className={classes.users}>
              <Link to="/api/info">
                <Icon className={classes.iconGroup}>group</Icon>
              </Link>
              <span className={classes.numberClients}>
                {appInfo.clients.length}
              </span>
            </div>
          </Typography>
          {renderRightNavbarMenu()}
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Navbar;
