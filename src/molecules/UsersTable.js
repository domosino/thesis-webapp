import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";
import React from "react";

const UsersTable = ({ clientsData }) => {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>#</TableCell>
          <TableCell>Username</TableCell>
          <TableCell>Country</TableCell>
          <TableCell>OS</TableCell>
          <TableCell>Browser</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {clientsData.map((tableRow, index) => {
          return (
            <TableRow key={index}>
              <TableCell component="th">{tableRow.index}</TableCell>
              <TableCell component="th">{tableRow.username}</TableCell>
              <TableCell component="th">{tableRow.country}</TableCell>
              <TableCell component="th">{tableRow.os}</TableCell>
              <TableCell component="th">{tableRow.browser}</TableCell>
            </TableRow>
          );
        })}
      </TableBody>
    </Table>
  );
};

export default UsersTable;
