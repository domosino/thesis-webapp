FROM node:alpine AS builder
WORKDIR /builder
COPY . /builder/
RUN sed 's/homepage\":.*/homepage\": \"\",/' -i package.json
RUN npm install
RUN npm run build

FROM nginx:alpine
COPY --from=builder /builder/build/ /usr/share/nginx/html
