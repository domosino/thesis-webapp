import { applyMiddleware, compose, createStore, combineReducers } from "redux";
import { createWebsocketMiddleware } from "@obl/redux-websocket-middleware";
import thunk from "redux-thunk";

import websocketReducer from "../reducers/websocket";
import authReducer from "../reducers/auth";
import salsaReducer from "../reducers/salsa";

import { isTokenExpired } from "../tokenService";
import jwtDecode from "jwt-decode";
import zmq2wsServiceInfoReducer from "../reducers/zmq2wsServiceInfo";

const reducer = combineReducers({
  appInfo: websocketReducer,
  auth: authReducer,
  zmq2wsServiceInfo: zmq2wsServiceInfoReducer,
  salsa: salsaReducer,
});

const token = localStorage.getItem("token");

const storeInitialState = {
  auth: {
    authenticated: token && isTokenExpired(token) ? "" : token,
    userData: token && jwtDecode(token),
  },
};

export default (initialState = storeInitialState) => {
  let wsUrl = window.env.API_WS_URL;
  // eslint-disable-next-line
  console.log(`Web socket server : ${wsUrl}`);

  const webSocketMiddleware = createWebsocketMiddleware({
    defaultEndpoint: wsUrl,
  });

  let middleware = applyMiddleware(thunk, webSocketMiddleware);

  if (process.env.NODE_ENV !== "production") {
    // eslint-disable-next-line
    console.log(`Api : ${window.env.API_URL}`);

    const devToolsExtension = window.devToolsExtension;
    if (typeof devToolsExtension === "function") {
      middleware = compose(middleware, devToolsExtension());
    }
  }

  const store = createStore(reducer, initialState, middleware);

  return store;
};
