export const getRedirectorUrl = (state) => state.salsa.redirectorUrl;
export const getSalsaName = (state) => state.salsa.name;
export const getSalsaSub = (state) => state.salsa.sub;
