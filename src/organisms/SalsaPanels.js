import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React from "react";
import { useSelector } from "react-redux";
import { getServiceData, getServiceSrc } from "../selectors/zmq2wsServiceInfo";
import SalsaInfo from "./SalsaInfo";
import SalsaSubmit from "../molecules/SalsaSubmit";
import SalsaTable from "./SalsaTable";

const useStyle = makeStyles((theme) => ({
  root: {
    width: "98vw",
    margin: "16px auto",
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    alignItems: "stretch",
  },
  wrapper: {
    width: "98%",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    margin: "0 auto",
    overflowX: "auto",
  },

  paper: {
    width: "100%",
    flex: 1,
    overflow: "hidden",
    margin: "auto",
  },

  expanded: {
    flex: 1,
    overflow: "hidden",
    margin: "auto !important",
    // "&:nth-child(1)": {
    //   minHeight: "calc(70vh - 96px)",
    // },
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },

  table: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  },
}));

const SalsaPanels = () => {
  const classes = useStyle();
  const serviceData = useSelector(getServiceData);
  const serviceSrc = useSelector(getServiceSrc);
  const [expanded, setExpanded] = React.useState(false);
  const [isInfoOpened, setIsInfoOpened] = React.useState(true);
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  React.useEffect(() => {
    handleChange("info")(null, true);
  }, []);

  if (serviceSrc !== "salsa" || serviceData.length === 0) {
    return null;
  }

  return (
    <div className={classes.root}>
      <ExpansionPanel
        className={classes.paper}
        expanded={isInfoOpened}
        onChange={() => setIsInfoOpened(!isInfoOpened)}
        classes={{ expanded: classes.expanded }}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>Info</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.table}>
          <SalsaInfo serviceData={serviceData} jobs={serviceData.jobs} />
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
        className={classes.paper}
        expanded={expanded === "queue"}
        onChange={handleChange("queue")}
        classes={{ expanded: classes.expanded }}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>Job queue</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.table}>
          {serviceData.jobs.length === 0 ? (
            <div style={{ textAlign: "center" }}>
              <h2>No jobs in queue</h2>
            </div>
          ) : (
            <Paper className={classes.wrapper}>
              <SalsaTable />
            </Paper>
          )}
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
        className={classes.paper}
        expanded={expanded === "submit"}
        onChange={handleChange("submit")}
        classes={{ expanded: classes.expanded }}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>Submit</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.table}>
          <SalsaSubmit
            openQueue={() => {
              handleChange("submit");
              handleChange("queue")(null, true);
            }}
          />
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
};

export default SalsaPanels;
