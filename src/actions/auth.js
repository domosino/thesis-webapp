import { AUTH_USER, AUTH_ERROR, LOADING_LOGIN } from "../actionTypes/auth";

export const loadingLogin = () => ({ type: LOADING_LOGIN });

export const authError = (error = "") => ({
  type: AUTH_ERROR,
  payload: error
});

export const removeToken = () => ({
  type: AUTH_USER,
  payload: ""
});
