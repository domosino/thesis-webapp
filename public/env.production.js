window.env = {
  // This option can be retrieved in "src/index.js" with "window.env.API_URL".
  API_URL: "localhost:8442/api",
  API_WS_URL: "ws://localhost:8442/api",
  HTTP_PROTOCOL: "http://",
  HTTP_REQUEST_TIMEOUT: "30000",
  REACT_APP_MAPS_KEY: " ",
};
