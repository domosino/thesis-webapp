import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { bytesToSize } from "../utils/utils";

const useStyle = makeStyles({
  container: {
    fontSize: "16px",
    padding: "5px",
  },
  value: {
    marginBottom: "16px",
    "&:last-child": {
      marginBottom: 0,
    },
  },
  adapter: {
    color: "#fff",
    fontWeight: "600",
  },
});

const GpusUsedTooltip = ({ gpus }) => {
  const classes = useStyle();
  return (
    <div className={classes.container}>
      {gpus &&
        gpus.map((node, index) => {
          return (
            <div key={index} className={classes.value}>
              <span className={classes.adapter}>{node.name}</span>:{" "}
              {bytesToSize(node.mem.used.value)}
            </div>
          );
        })}
    </div>
  );
};

export default GpusUsedTooltip;
