export const getAppInfo = (state) => state.appInfo;
export const getWsConnected = (state) => state.appInfo.wsConnected;
export const getWebsocketId = (state) => state.appInfo.id;
