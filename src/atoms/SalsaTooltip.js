import { makeStyles } from "@material-ui/core";
import React from "react";
import { arrayToRangeString } from "../utils/utils";

const useStyle = makeStyles({
  container: {
    fontSize: "16px",
    padding: "5px",
  },
  value: {
    marginBottom: "16px",
    "&:last-child": {
      marginBottom: 0,
    },
  },
  adapter: {
    color: "#fff",
    fontWeight: "600",
  },
});

const SalsaTooltip = ({ data }) => {
  const classes = useStyle();
  return (
    <div className={classes.container}>
      <div className={classes.value}>
        <span className={classes.adapter}>{`[${arrayToRangeString(
          data
        )}]`}</span>
      </div>
    </div>
  );
};

export default SalsaTooltip;
