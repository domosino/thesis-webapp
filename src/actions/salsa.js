import {
  ON_SALSA_CHANGE,
  ON_SALSA_RESET,
  ON_SALSA_NAME_CHANGE,
  ON_SALSA_SUB_CHANGE,
  ON_SALSA_DATA_CHANGE,
} from "../actionTypes/salsa";

export const onSalsaChange = (data) => ({
  type: ON_SALSA_CHANGE,
  data,
});

export const onSalsaNameChange = (name) => ({
  type: ON_SALSA_NAME_CHANGE,
  name,
});

export const onSalsaSubChange = (sub) => ({
  type: ON_SALSA_SUB_CHANGE,
  sub,
});

export const onSalsaDataChange = (data) => ({
  type: ON_SALSA_DATA_CHANGE,
  data,
});

export const onSalsaReset = () => ({
  type: ON_SALSA_RESET,
});
