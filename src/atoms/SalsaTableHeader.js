import { makeStyles, TableHead, TableRow } from "@material-ui/core";
import React from "react";
import TableCell from "./TableCell";

const tableHeaders = [
  { id: 1, name: "Name", colSpan: 1 },
  { id: 2, name: "User", colSpan: 1 },
  { id: 3, name: "Group", colSpan: 1 },
  { id: 4, name: "Total", colSpan: 1 },
  { id: 5, name: "Pending", colSpan: 1 },
  { id: 6, name: "Running", colSpan: 1 },
  { id: 7, name: "Finished", colSpan: 1 },
  { id: 8, name: "Failed", colSpan: 1 },
  { id: 9, name: "Status", colSpan: 1 },
  { id: 10, name: "Started", colSpan: 1 },
  { id: 11, name: "Running time", colSpan: 1 },
  { id: 12, name: "Estimated time", colSpan: 1 },
  { id: 13, name: "Finished", colSpan: 1 },
  { id: 14, name: "Action", colSpan: 1 },
];

const useStyle = makeStyles({
  cell: {
    textAlign: "center",
    borderLeft: "1px solid #ccc",
    width: "50px",
    fontSize: "13px",
  },
});

const TableHeader = () => {
  const classes = useStyle();

  return (
    <TableHead>
      <TableRow>
        {tableHeaders.map((header) => {
          return (
            <TableCell
              key={header.id}
              colSpan={header.colSpan}
              className={classes.cell}
            >
              {header.name}
            </TableCell>
          );
        })}
      </TableRow>
    </TableHead>
  );
};

export default TableHeader;
