import { Button, Grid, makeStyles, TextField } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { getRedirectorUrl } from "../selectors/salsa";
import axios from "axios";
import { getUserData, getAuthenticatedToken } from "../selectors/auth";

const useStyle = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  form: {
    width: "100%",
    display: "flex",
  },

  command: {
    width: "100%",
  },
  times: {
    width: "100%",
  },
  button: {
    width: "100%",
  },
  code: {
    textAlign: "left",
    width: "100%",
    minHeight: "2rem",
    backgroundColor: "#000",
    color: "#fff",
    borderRadius: "10px",
    marginTop: theme.spacing(2),
    fontSize: "1.05rem",
    fontFamily: "monospace, monospace",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: "1rem",
  },
  alert: {
    borderRadius: "10px",
    marginTop: theme.spacing(2),
  },
}));

const SalsaSubmit = ({ openQueue }) => {
  const commandRef = useRef();
  const classes = useStyle();
  const commandInput = useInput("sleep 1");
  const timesInput = useInput(1000);
  const [isInvalid, setIsInvalid] = useState(false);
  const [isCopied, setIsCopied] = useState(false);
  const redirectorUrl = useSelector(getRedirectorUrl);
  const authenticatedToken = useSelector(getAuthenticatedToken);
  const userData = useSelector(getUserData);

  useEffect(() => {
    if (commandInput.value !== "" && timesInput.value !== "") {
      setIsInvalid(false);
    } else {
      setIsInvalid(true);
    }
  }, [commandInput.value, timesInput]);

  const executeJob = async () => {
    const cmd = `salsa-feeder -s ${redirectorUrl} -t "${commandInput.value}:${timesInput.value}"`;
    const {
      profile: { uidNumber, gidNumber },
    } = userData;

    try {
      console.log(window.env.API_URL);
      const res = await axios.post(
        `${window.env.HTTP_PROTOCOL}${window.env.API_URL}/salsa/exec`,
        {
          cmd: cmd,
          uid: parseInt(uidNumber),
          gid: parseInt(gidNumber),
        }
      );
      console.log(res);
      openQueue();
    } catch (err) {
      console.error(err);
    }
  };

  const copyToClipboard = async () => {
    await navigator.clipboard.writeText(commandRef.current.textContent);
    setIsCopied(true);
    setTimeout(() => setIsCopied(false), 1500);
  };

  if (!redirectorUrl) {
    return (
      <Alert severity="error">Cluster info is missing ({redirectorUrl})</Alert>
    );
  }

  return (
    <div className={classes.wrapper}>
      <div className={classes.form}>
        <Grid container spacing={3}>
          <Grid item xs={12} md={9}>
            <TextField
              className={classes.command}
              label="Command"
              required
              {...commandInput}
            />
          </Grid>
          <Grid item xs={6} md={1}>
            <TextField
              className={classes.times}
              label="Times"
              type="number"
              required
              inputProps={{ style: { textAlign: "right" } }}
              {...timesInput}
            />
          </Grid>
          <Grid item xs={6} md={2}>
            <Button
              className={classes.button}
              onClick={executeJob}
              variant="contained"
              color="primary"
              disabled={isInvalid || authenticatedToken === ""}
            >
              Execute job
            </Button>
          </Grid>
        </Grid>
      </div>
      {isCopied ? (
        <Alert className={classes.alert} severity="success">
          Copied to clipboard
        </Alert>
      ) : (
        <Alert className={classes.alert} severity="info">
          Click on command to copy to clipboard
        </Alert>
      )}
      <p className={classes.code} ref={commandRef} onClick={copyToClipboard}>
        {`salsa-feeder -s ${redirectorUrl} -t "${commandInput.value}:${timesInput.value}"`}
      </p>
    </div>
  );
};

const useInput = (initialValue) => {
  const [value, setValue] = useState(initialValue);
  const onChange = (e) => setValue(e.target.value);
  return { value, onChange };
};

export default SalsaSubmit;
