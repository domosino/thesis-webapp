import { ActionTypes } from "@obl/redux-websocket-middleware";
import {
  ON_SALSA_CHANGE,
  ON_SALSA_RESET,
  ON_SALSA_NAME_CHANGE,
  ON_SALSA_SUB_CHANGE,
  ON_SALSA_DATA_CHANGE,
} from "../actionTypes/salsa";

const salsaDefaultState = {
  name: "",
  sub: "",
  redirectorUrl: "",
  slots: 0,
};

export default (state = salsaDefaultState, action) => {
  switch (action.type) {
    case ActionTypes.RECEIVED_WEBSOCKET_DATA: {
      const { type } = action.payload;
      if (type === "zmq") {
        if (
          action.payload.data.name === state.name &&
          action.payload.data.sub === state.sub &&
          action.payload.src === "salsa"
        ) {
          if (action.payload.data.data.length !== 0) {
            return {
              ...state,
              redirectorUrl: action.payload.data.data.node.submitUrl,
              slots: action.payload.data.data.node.slots,
            };
          } else {
            return state;
          }
        }
      }
      return state;
    }
    case ON_SALSA_CHANGE:
      return { ...action.data };
    case ON_SALSA_NAME_CHANGE:
      return { ...salsaDefaultState, name: action.name };
    case ON_SALSA_SUB_CHANGE:
      return { ...state, sub: action.sub };
    case ON_SALSA_DATA_CHANGE:
      return {
        ...state,
        redirectorUrl: action.redirectorUrl,
        slots: action.slots,
      };
    case ON_SALSA_RESET:
      return salsaDefaultState;
    default:
      return state;
  }
};
