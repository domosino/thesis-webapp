import { makeStyles, TableBody, TableCell, TableRow } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import Button from "../atoms/Button";
import { getAuthenticatedToken } from "../selectors/auth";
import averageTaskList from "../utils/salsa/averageTaskList";
import taskCellList from "../utils/salsa/taskCellList";
import totalTaskList from "../utils/salsa/totalTaskList";

const useStyle = makeStyles((theme) => ({
  root: {
    width: "98%",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    margin: "0 auto",
    overflowX: "auto",
  },
  idCell: {
    width: "50px",
  },
  table: {
    // minWidth: 1000
  },
  row: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default,
    },
  },
  cell: {
    textAlign: "center",
    borderLeft: "1px solid #ccc",
    width: "50px",
    fontSize: "13px",
  },
  bar: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "20px",
  },
  total: {
    paddingLeft: "20px",
    fontSize: "16px",
    backgroundColor: "#EDEDED",
  },
  widthCell: {
    width: "200px",
    textAlign: "center",
  },
}));

const SalsaTableContent = ({ tasksData }) => {
  const classes = useStyle();
  const auth = useSelector(getAuthenticatedToken);

  return (
    <TableBody>
      {tasksData.map((task) => (
        <TableRow key={task.name} className={classes.row}>
          {taskCellList(task, classes, tasksData).map((cell) => (
            <TableCell key={cell.id} {...cell} />
          ))}
          <TableCell className={classes.widthCell}>
            {auth && (
              <Button
                task={task}
                jobId={task.name}
                children={task.percentage === 100 ? "Remove" : "Cancel"}
              />
            )}
          </TableCell>
        </TableRow>
      ))}
      <TableRow className={classes.row}>
        {totalTaskList(tasksData, classes).map((cell) => (
          <TableCell key={cell.id} {...cell} />
        ))}
        <TableCell className={classes.widthCell}>
          {auth && <Button children="Stop all" />}
        </TableCell>
      </TableRow>
      <TableRow className={classes.row}>
        {averageTaskList(tasksData, classes).map((cell) => (
          <TableCell key={cell.id} {...cell} />
        ))}
      </TableRow>
    </TableBody>
  );
};

export default SalsaTableContent;
