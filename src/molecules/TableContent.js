import { makeStyles, TableBody, TableCell, TableRow } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import { getServiceData } from "../selectors/zmq2wsServiceInfo";
import averageRowList from "../utils/obmon/averageRowList";
import generateRowData from "../utils/obmon/generateRowData";
import gpuTableCellList from "../utils/obmon/gpuTableCellList";
import tableCellList from "../utils/obmon/tableCellList";
import totalRowList from "../utils/obmon/totalRowList";
import TableSubHeader from "../atoms/TableSubHeader";

const useStyle = makeStyles((theme) => ({
  root: {
    width: "98%",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    margin: "0 auto",
    overflowX: "auto",
  },
  idCell: {
    width: "50px",
  },
  table: {
    // minWidth: 1000
  },
  row: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default,
    },
  },
  cell: {
    textAlign: "center",
    borderLeft: "1px solid #ccc",
    width: "50px",
    fontSize: "13px",
  },
  bar: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "20px",
  },
  total: {
    paddingLeft: "20px",
    fontSize: "16px",
    backgroundColor: "#EDEDED",
  },
  widthCell: {
    width: "200px",
  },
}));

const TableContent = () => {
  const classes = useStyle();
  const serviceData = useSelector(getServiceData);
  const tableRowsData = serviceData.map((row, index) =>
    generateRowData(row, serviceData, index + 1)
  );
  const findInsideTableRows = tableRowsData.find((el) =>
    el.loadGpu ? true : false
  );

  return (
    <TableBody>
      <TableSubHeader />
      {tableRowsData.map((row) => {
        return (
          <TableRow key={row.uuid} className={classes.row}>
            {tableCellList(row, classes).map((cell) => (
              <TableCell key={cell.id} {...cell} />
            ))}

            {findInsideTableRows &&
              gpuTableCellList(row, classes).map((cell) => (
                <TableCell key={cell.id} {...cell} />
              ))}
          </TableRow>
        );
      })}
      {tableRowsData.length > 0 && (
        <React.Fragment>
          <TableRow>
            {totalRowList(tableRowsData, classes, findInsideTableRows).map(
              (cell) => (
                <TableCell key={cell.id} {...cell} />
              )
            )}
          </TableRow>
          <TableRow>
            {averageRowList(tableRowsData, classes, findInsideTableRows).map(
              (cell) => (
                <TableCell key={cell.id} {...cell} />
              )
            )}
          </TableRow>
        </React.Fragment>
      )}
    </TableBody>
  );
};

export default TableContent;
