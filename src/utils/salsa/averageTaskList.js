import { v4 } from "uuid";
import pretty from "pretty-time";

const averageTaskList = (tasks, classes) => [
  {
    id: v4(),
    className: classes.total,
    colSpan: 1,
    children: `Average`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: Math.round(
      tasks.reduce((sum, task) => sum + task.total, 0) / tasks.length
    )
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: Math.round(
      tasks.reduce((sum, task) => sum + task.pending, 0) / tasks.length
    )
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: Math.round(
      tasks.reduce((sum, task) => sum + task.running, 0) / tasks.length
    )
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: Math.round(
      tasks.reduce((sum, task) => sum + task.finished, 0) / tasks.length
    )
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: Math.round(
      tasks.reduce((sum, task) => sum + task.failed, 0) / tasks.length
    )
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: pretty(
      [
        tasks.reduce((sum, task) => sum + task.running_time / task.total, 0) /
          tasks.length,
        1e-9
      ],
      "ms"
    )
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `-`
  }
];

export default averageTaskList;
