import jwtDecode from "jwt-decode";

const absVal = (integer) => {
    return integer < 0 ? -integer : integer;
};

export const isTokenExpired = (token) => {

    if (token) {

        const decodedToken = jwtDecode(token);
        let expirationDate = decodedToken.exp;

        if (!expirationDate) {
            new Error("No expiration date");
            return true;
        }

        const exp = new Date(expirationDate).valueOf();
        const now = new Date().valueOf() / 1000;
        const iat = decodedToken.iat;
        const fiveSeconds = 5000;

        if (absVal(iat - now) >= fiveSeconds) {
            return true;
        }

        return exp < now;
    } else {
        return true;
    }

};
