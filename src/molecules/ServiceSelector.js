import { Chip, CircularProgress, makeStyles } from "@material-ui/core";
import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation, useParams } from "react-router-dom";
import {
  onSalsaNameChange,
  onSalsaReset,
  onSalsaSubChange,
} from "../actions/salsa";
import { writeToSocket } from "../actions/websocket";
import {
  getData,
  getIsSubscribed,
  getNameService,
  getSubService,
} from "../actions/zmq2wsServiceInfo";
import { getSalsaName, getSalsaSub } from "../selectors/salsa";
import { getAppInfo, getWsConnected } from "../selectors/websocket";
import {
  getServiceIsSub,
  getServiceName,
  getServiceSrc,
  getServiceSub,
} from "../selectors/zmq2wsServiceInfo";

const useStyle = makeStyles({
  wrapper: {
    padding: "25px 10px 0px 10px",
  },
  chipContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexWrap: "wrap",
  },
  chip: {
    margin: "1rem",
    width: "100px",
    fontSize: "14px",
    userSelect: "none",
  },
});

const getUniqueNames = (services, pathName) => {
  let serviceNames = services
    .filter((service) => service.src === pathName)
    .map((service) => service.name);
  return serviceNames.filter(
    (name, index) => serviceNames.indexOf(name) === index
  );
};

const getFilteredServices = (services, tableName) => {
  return services.filter((service) => service.name === tableName);
};

const ServiceSelector = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { services } = useSelector(getAppInfo);
  const serviceName = useSelector(getServiceName);
  const serviceSub = useSelector(getServiceSub);
  const serviceSrc = useSelector(getServiceSrc);
  const isSub = useSelector(getServiceIsSub);
  const salsaName = useSelector(getSalsaName);
  const salsaSub = useSelector(getSalsaSub);
  const wsConnected = useSelector(getWsConnected);
  const { pathname } = useLocation();
  const pathName = pathname.split("/")[1];
  const classes = useStyle();
  const { service, sub } = useParams();

  const uniqueNames = getUniqueNames(services, pathName);
  const filteredServices = getFilteredServices(services, serviceName);

  React.useEffect(() => {
    dispatch(onSalsaReset());
    dispatch(getSubService(""));
    dispatch(getNameService(""));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    if (!salsaName) return;
    if (!salsaSub) return;
    if (!serviceSub) return;

    if (!isSub) {
      dispatch(
        writeToSocket({
          type: "sub",
          name: salsaName,
          sub: salsaSub,
          src: pathName,
        })
      );
      dispatch(getIsSubscribed(true));
    } else if (isSub && salsaSub !== serviceSub) {
      return;
    }
    return () => {
      if (
        salsaName &&
        salsaSub &&
        salsaName === serviceName &&
        serviceSrc !== ""
      ) {
        if (isSub && serviceSrc === pathName) {
          dispatch(getIsSubscribed(false));
          dispatch(
            writeToSocket({
              type: "unsub",
              name: salsaName,
              sub: salsaSub,
              src: pathName,
            })
          );
          dispatch(getData([]));
        }
      }
    };
  }, [
    dispatch,
    salsaName,
    salsaSub,
    pathName,
    serviceName,
    serviceSub,
    serviceSrc,
    isSub,
  ]);

  const handleChipClick = useCallback(
    (e) => {
      dispatch(getIsSubscribed(false));
      dispatch(onSalsaSubChange(""));
      dispatch(onSalsaNameChange(e.target.textContent));
      dispatch(getSubService(""));
      dispatch(getNameService(e.target.textContent));
    },
    [dispatch]
  );

  const handleTabClick = useCallback(
    (event, service) => {
      const sub = event.target.textContent;
      if (serviceSub !== sub) dispatch(getSubService(sub));
      if (salsaSub !== sub) dispatch(onSalsaSubChange(sub));
    },
    [dispatch, serviceSub, salsaSub]
  );

  React.useEffect(() => {
    if (service && sub && wsConnected) {
      setTimeout(() => {
        const serviceEvent = { target: { textContent: service } };
        const subEvent = { target: { textContent: sub } };
        handleChipClick(serviceEvent);
        handleTabClick(subEvent);
      }, 0);
    } else if (service && !sub && wsConnected) {
      const serviceEvent = { target: { textContent: service } };
      handleChipClick(serviceEvent);
    } else if (!service || !sub) {
      dispatch(onSalsaReset());
      dispatch(getSubService(""));
      dispatch(getNameService(""));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, service, sub, wsConnected]);

  return (
    <div className={classes.wrapper}>
      <div className={classes.chipContainer}>
        {uniqueNames.length === 0 ? (
          <CircularProgress size={40} />
        ) : (
          uniqueNames.map((name, index) => (
            <Chip
              onClick={() =>
                history.push(
                  `/${history.location.pathname.split("/")[1]}/${name}`
                )
              }
              key={index}
              label={name}
              className={classes.chip}
              style={{ backgroundColor: name === serviceName && "#039BE5" }}
            />
          ))
        )}
      </div>
      <div className={classes.chipContainer}>
        {serviceName.length !== 0 && (
          <React.Fragment>
            {filteredServices.map((service, index) => (
              <Chip
                onClick={() => {
                  const splitted = history.location.pathname.split("/");
                  const { length } = splitted;
                  if (length === 3) {
                    history.push(`${history.location.pathname}/${service.sub}`);
                  } else {
                    const newPath = `${history.location.pathname
                      .split("/")
                      .slice(0, 3)
                      .join("/")}/${service.sub}`;
                    history.push(newPath);
                  }
                }}
                key={index}
                label={service.sub}
                className={classes.chip}
                style={{
                  backgroundColor: service.sub === serviceSub && "#039BE5",
                }}
              />
            ))}
          </React.Fragment>
        )}
      </div>
    </div>
  );
};

export default ServiceSelector;
