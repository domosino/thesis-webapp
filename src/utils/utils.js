export const precisionRound = (number, precision) => {
  const factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
};

export const bytesToSize = (bytes) => {
  const sizes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
  if (bytes < 1) {
    return "1 B";
  }
  if (bytes === 0) {
    return "0 B";
  }
  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);
  if (i === 0) {
    return bytes + " " + sizes[i];
  }
  return (bytes / Math.pow(1024, i)).toFixed(2) + " " + sizes[i];
};

export const bytesToSizeInSeconds = (bytes) => {
  const sizes = [
    "B/s",
    "KB/s",
    "MB/s",
    "GB/s",
    "TB/s",
    "PB/s",
    "EB/s",
    "ZB/s",
    "YB/s",
  ];
  if (bytes < 1) {
    return "-";
  }
  if (bytes === 0) {
    return "0 B/s";
  }
  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);
  if (i === 0) {
    return bytes + " " + sizes[i];
  }
  return (
    decimalPlace(Number((bytes / Math.pow(1024, i)).toFixed(2)), 2) +
    " " +
    sizes[i]
  );
};

export const decimalPlace = (num, decimal, alpha) => {
  if (alpha) {
    return (
      Number(parseFloat(Math.round(num * 100) / 100).toFixed(decimal)) / 100
    );
  }
  return Number(parseFloat(Math.round(num * 100) / 100).toFixed(decimal));
};

export const readableByteInSeconds = (bytes) => {
  if (bytes === null) {
    return "-";
  }
  const sizes = [
    "B/s",
    "KB/s",
    "MB/s",
    "GB/s",
    "TB/s",
    "PB/s",
    "EB/s",
    "ZB/s",
    "YB/s",
  ];
  if (bytes === 0) {
    return "0 B/s";
  }
  if (bytes) {
    const i = Math.floor(Math.log(bytes) / Math.log(1024));
    return Number((bytes / Math.pow(1024, i)).toFixed(2)) * 1 + " " + sizes[i];
  }
};

export const removeDuplicates = (array, property) => {
  return array.filter((object, position, arr) => {
    return (
      arr.map((mapObj) => mapObj[property]).indexOf(object[property]) ===
      position
    );
  });
};

export const bytesInDisk = (data) => {
  if (!data.sys.change.disks) {
    return null;
  }
  if (data.sys.change.disks.length !== 0) {
    const bytesInSum = data.sys.change.disks.reduce(
      (acc, value) => acc + value.bytes_read.value,
      0
    );

    const bytesInSumAlpha = data.sys.change.disks.reduce(
      (acc, value) => acc + value.bytes_read.alpha,
      0
    );
    return {
      value: bytesInSum,
      alpha: bytesInSumAlpha,
      disks: data.sys.change.disks,
    };
  } else {
    return null;
  }
};

export const bytesOutDisk = (data) => {
  if (!data.sys.change.disks) {
    return null;
  }
  if (data.sys.change.disks.length !== 0) {
    const bytesOutSum = data.sys.change.disks.reduce(
      (acc, value) => acc + value.bytes_write.value,
      0
    );

    const bytesOutSumAlpha = data.sys.change.disks.reduce(
      (acc, value) => acc + value.bytes_write.alpha,
      0
    );
    return {
      value: bytesOutSum,
      alpha: bytesOutSumAlpha,
      disks: data.sys.change.disks,
    };
  }

  return null;
};

export const bytesIn = (data) => {
  const bytesInSum = data.sys.change.net
    .filter((network) => network.adapter !== "lo")
    .reduce((acc, value) => {
      return acc + value.bytes_in.value;
    }, 0);

  const bytesInAlpha = data.sys.change.net
    .filter((network) => network.adapter !== "lo")
    .reduce((acc, value) => {
      return acc + value.bytes_in.alpha;
    }, 0);

  return {
    value: bytesInSum,
    alpha: bytesInAlpha,
    net: data.sys.change.net,
  };
};

export const bytesOut = (data) => {
  const bytesOutSum = data.sys.change.net
    .filter((network) => network.adapter !== "lo")
    .reduce((acc, value) => {
      return acc + value.bytes_out.value;
    }, 0);

  const bytesOutAlpha = data.sys.change.net
    .filter((network) => network.adapter !== "lo")
    .reduce((acc, value) => {
      return acc + value.bytes_out.alpha;
    }, 0);

  return {
    value: bytesOutSum,
    alpha: bytesOutAlpha,
    net: data.sys.change.net,
  };
};

export const GpuLoad = (data) => {
  const load = data.gpu.value.gpus.reduce((acc, value) => {
    return acc + value.load;
  }, 0);

  return {
    load: load,
    gpus: data.gpu.value.gpus,
  };
};

export const GpuUsedMemory = (data) => {
  const usedMemory = data.gpu.value.gpus.reduce((acc, value) => {
    return acc + value.mem.used.value;
  }, 0);

  const usedMemoryAlpha = data.gpu.value.gpus.reduce((acc, value) => {
    return acc + value.mem.used.alpha;
  }, 0);

  return {
    used: usedMemory,
    alpha: usedMemoryAlpha,
    gpus: data.gpu.value.gpus,
  };
};

export const GpuTotalMemory = (data) => {
  const totalMemory = data.gpu.value.gpus.reduce((acc, value) => {
    return acc + value.mem.total.value;
  }, 0);

  return {
    total: totalMemory,
    gpus: data.gpu.value.gpus,
  };
};

export const calculatedUsedValue = (data) => {
  return {
    value: data.used.value - data.buffer.value - data.cached.value,
    alpha: data.used.alpha - data.buffer.alpha - data.cached.alpha,
  };
};

export const generateBackgroundColor = (cores, load) => {
  if (cores === 0 || cores === undefined) {
    return "rgba(3,155,229,0)";
  }
  const value = load / cores;
  if (value > 1) {
    return "rgba(3,155,229,1)";
  }
  return `rgba(3,155,229, ${value})`;
};

export const generateCpuAverage = (tableRows, property) => {
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const total = tableRows
    .map((tableRow) => tableRow[property].value)
    .reduce(reducer);
  return decimalPlace(total / tableRows.length, 1);
};

export const generateMemoryAverageAndTotal = (tableRows, property, type) => {
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const total = tableRows
    .map((tableRow) => tableRow[property].value)
    .reduce(reducer);
  if (type === "total") {
    return bytesToSize(total);
  }

  return bytesToSize(total / tableRows.length);
};

export const generateNetworkAverageAndTotal = (tableRows, property, type) => {
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const total = tableRows
    .map((tableRow) => tableRow[property].value)
    .reduce(reducer);
  if (type === "total") {
    return bytesToSizeInSeconds(total);
  }

  return bytesToSizeInSeconds(Math.floor(total / tableRows.length));
};

export const generateDisksAverageAndTotal = (tableRows, property, type) => {
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const total = tableRows
    .map((tableRow) => tableRow[property] && tableRow[property].value)
    .reduce(reducer);
  if (type === "total") {
    return readableByteInSeconds(total);
  }
  return readableByteInSeconds(Math.floor(total / tableRows.length));
};

export const generateTotalNumberLoad1 = (tableRows, property, type) => {
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const totalLoad1 = tableRows
    .map((tableRow) => tableRow[property])
    .reduce(reducer);
  if (type === "total") {
    return decimalPlace(totalLoad1, 2);
  }

  return decimalPlace(totalLoad1 / tableRows.length, 2);
};

export const calculatedCoresTotalAndAverage = (tableRows, property, type) => {
  const numberOfCores = tableRows.filter(
    (tableRow) => tableRow.cores !== undefined
  ).length;
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const totalCores = tableRows
    .filter((tableRow) => tableRow[property] !== undefined)
    .map((tableRow) => tableRow[property])
    .reduce(reducer);

  if (type === "total") {
    return totalCores;
  }

  return decimalPlace(totalCores / numberOfCores, 1);
};

export const arrayToRangeString = (vector) => {
  const l = vector.length - 1;
  vector = vector.map((n, i, v) =>
    i < l && v[i + 1] - n === 1
      ? `${i > 0 && n - v[i - 1] === 1 ? "" : n}-`
      : `${n};`
  );
  return vector.join("").replace(/-+/g, "-").slice(0, -1);
};
