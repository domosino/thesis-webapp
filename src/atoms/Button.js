import { Button } from "@material-ui/core";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getUserData } from "../selectors/auth";
import { getRedirectorUrl } from "../selectors/salsa";

export default ({ children, jobId, task }) => {
  const [jobStopped, setJobStopped] = useState(false);
  const [uid, setUid] = useState(-1);
  const userData = useSelector(getUserData);
  const redirectorUrl = useSelector(getRedirectorUrl);

  useEffect(() => {
    if (userData) {
      const { profile } = userData;
      setUid(parseInt(profile.uidNumber));
    }
  }, [userData]);

  const makeRequest = async () => {
    setJobStopped(true);
    if (jobId) {
      await axios.post(
        `${window.env.HTTP_PROTOCOL}${window.env.API_URL}/salsa/cmd`,
        {
          url: redirectorUrl,
          cmd: "JOB_DEL_ID",
          arg: jobId,
        }
      );
    } else {
      await axios.post(
        `${window.env.HTTP_PROTOCOL}${window.env.API_URL}/salsa/cmd`,
        {
          url: redirectorUrl,
          cmd: "JOB_DEL_ALL",
        }
      );
    }
  };

  if (task && task.uid !== uid) {
    return null;
  }

  return (
    <Button
      onClick={() => makeRequest()}
      variant="contained"
      color="primary"
      disabled={jobStopped}
    >
      {jobStopped ? "Removing ..." : children}
    </Button>
  );
};
