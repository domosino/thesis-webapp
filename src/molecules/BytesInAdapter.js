import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { readableByteInSeconds } from "../utils/utils";

const useStyle = makeStyles({
  container: {
    fontSize: "16px",
    padding: "5px",
  },
  value: {
    marginBottom: "16px",
    "&:last-child": {
      marginBottom: 0,
    },
  },
  adapter: {
    color: "#fff",
    fontWeight: "600",
  },
});

const BytesInAdapter = ({ adapter }) => {
  const classes = useStyle();
  return (
    <div className={classes.container}>
      {adapter.bytesIn.net.map((node) => {
        if (node.adapter === "lo") {
          return null;
        }

        return (
          <div key={node.adapter} className={classes.value}>
            <span className={classes.adapter}>{node.adapter}</span>:{" "}
            {readableByteInSeconds(node.bytes_in.value)}
          </div>
        );
      })}
    </div>
  );
};

export default BytesInAdapter;
