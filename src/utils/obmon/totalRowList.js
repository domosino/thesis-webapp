import {
  calculatedCoresTotalAndAverage,
  generateTotalNumberLoad1,
  generateMemoryAverageAndTotal,
  generateDisksAverageAndTotal,
  generateNetworkAverageAndTotal
} from "../utils";
import { v4 } from "uuid";

const totalRowList = (tableRowsData, classes, findInsideTableRows) => [
  {
    id: v4(),
    className: classes.total,
    colSpan: 2,
    children: `Total`
  },
  {
    id: v4(),
    children: calculatedCoresTotalAndAverage(tableRowsData, "cores", "total")
  },
  {
    id: v4(),
    children: generateTotalNumberLoad1(tableRowsData, "load1", "total")
  },
  { id: v4(), children: `-` },
  { id: v4(), children: `-` },
  { id: v4(), children: `-` },
  { id: v4(), children: `-` },
  { id: v4(), children: `-` },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateMemoryAverageAndTotal(tableRowsData, "used", "total")
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateMemoryAverageAndTotal(tableRowsData, "cached", "total")
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateMemoryAverageAndTotal(
      tableRowsData,
      "totalMEMORY",
      "total"
    )
  },
  {
    id: v4(),
    children: generateDisksAverageAndTotal(tableRowsData, "disk_write", "total")
  },
  {
    id: v4(),
    children: generateDisksAverageAndTotal(tableRowsData, "disk_read", "total")
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateNetworkAverageAndTotal(tableRowsData, "bytesIn", "total")
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateNetworkAverageAndTotal(tableRowsData, "bytesOut", "total")
  },
  ...(findInsideTableRows
    ? ["", "", "", ""].map(x => ({ id: v4(), children: `-` }))
    : [])
];

export default totalRowList;
