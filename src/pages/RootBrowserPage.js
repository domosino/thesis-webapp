import {
  Button,
  Dialog,
  IconButton,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import { JSRootBrowser } from "@obl/react-jsroot";
import React, { Fragment, useState } from "react";
import "./RootBrowser.css";

const useStyle = makeStyles((theme) => ({
  root: {
    width: "95%",
    margin: "auto",
    marginTop: theme.spacing(1),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  textField: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: "90%",
  },
  button: {
    margin: theme.spacing(1),
    width: 100,
    marginLeft: 10,
  },
  rootBrowser: {
    padding: theme.spacing(3),
  },
  dialogTitle: {
    textAlign: "center",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(2),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}));

const RootBrowserPage = () => {
  const classes = useStyle();
  const [filename, setFilename] = useState(
    "https://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/scratch/hsimple.root"
  );
  const [isSubmitted, setIsSubmitted] = useState(false);

  // const renderRootBrowser = () => {
  //   if (!isSubmitted) {
  //     return null;
  //   }

  //   if (filename === "") {
  //     return <Alert severity="error">Expected a filename</Alert>;
  //   }

  //   return (
  //     <Dialog open={isDialogOpen} onClose={() => setIsDialogOpen(false)}>
  //       <DialogTitle>ROOT Browser</DialogTitle>
  //       <JSRootBrowser
  //         name="MyBrows"
  //         filename={filename}
  //         obj="hpxpy;1"
  //         drawOptions="colz"
  //         displayType="simple"
  //       />
  //     </Dialog>
  //   );
  // };

  return (
    <Fragment>
      <div className={classes.root}>
        <TextField
          className={classes.textField}
          id="filename"
          onChange={(e) => setFilename(e.target.value)}
          value={filename}
          placeholder="URL to ROOT file"
        />
        <Button
          variant="outlined"
          color="primary"
          className={classes.button}
          onClick={() => setIsSubmitted(true)}
        >
          Open
        </Button>
      </div>
      <Dialog
        fullWidth={true}
        maxWidth={"lg"}
        open={isSubmitted}
        onClose={() => setIsSubmitted(false)}
      >
        <MuiDialogTitle className={classes.dialogTitle} disableTypography>
          <Typography variant="h6">ROOT Browser</Typography>
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={() => setIsSubmitted(false)}
          >
            <CloseIcon />
          </IconButton>
        </MuiDialogTitle>
        <JSRootBrowser
          className={classes.rootBrowser}
          name="MyBrows"
          filename={filename}
          obj="hpxpy;1"
          drawOptions="colz"
          displayType="simple"
        />
      </Dialog>
    </Fragment>
  );
};

export default RootBrowserPage;
