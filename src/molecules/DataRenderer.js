import {
  Checkbox,
  FormControlLabel,
  makeStyles,
  Paper,
  Table,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getServiceData, getServiceSrc } from "../selectors/zmq2wsServiceInfo";
import TableContent from "./TableContent";
import TableHeader from "../atoms/TableHeader";

const useStyle = makeStyles((theme) => ({
  wrapper: {
    width: "98%",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    margin: "0 auto",
    overflowX: "auto",
  },
}));

const DataRenderer = () => {
  const classes = useStyle();
  const serviceData = useSelector(getServiceData);
  const serviceSrc = useSelector(getServiceSrc);
  const dense = localStorage.getItem("isDenseTable");
  const [isDense, setIsDense] = useState(dense === "true" ? true : false);

  useEffect(() => {
    localStorage.setItem("isDenseTable", isDense);
  }, [isDense]);

  if (serviceSrc !== "obmon") {
    return null;
  }

  if (serviceData.length === 0) {
    return null;
  }

  return (
    <Paper className={classes.wrapper}>
      <FormControlLabel
        control={
          <Checkbox
            checked={isDense}
            onChange={(e) => setIsDense(e.target.checked)}
          />
        }
        color="primary"
        label="Show dense table"
      />
      <Table size={isDense ? "small" : "medium"}>
        <TableHeader />
        <TableContent />
      </Table>
    </Paper>
  );
};

export default DataRenderer;
