import { makeStyles } from "@material-ui/core";
import React from "react";
import { readableByteInSeconds } from "../utils/utils";

const useStyle = makeStyles({
  container: {
    fontSize: "16px",
    padding: "5px",
  },
  value: {
    marginBottom: "16px",
    "&:last-child": {
      marginBottom: 0,
    },
  },
  adapter: {
    color: "#fff",
    fontWeight: "600",
  },
});

const DiskWrite = ({ disks }) => {
  const classes = useStyle();
  return (
    <div className={classes.container}>
      {disks &&
        disks.map((node) => {
          return (
            <div key={node.deviceName} className={classes.value}>
              <span className={classes.adapter}>{node.deviceName}</span>:{" "}
              {readableByteInSeconds(node.bytes_write.value)}
            </div>
          );
        })}
    </div>
  );
};

export default DiskWrite;
