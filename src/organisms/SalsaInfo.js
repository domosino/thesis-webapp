import { LinearProgress, makeStyles, Paper, Tooltip } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import React from "react";
import SalsaHosts from "../molecules/SalsaHosts";

const useStyle = makeStyles((theme) => ({
  wrapper: {
    width: "98%",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    margin: "0 auto",
    overflowX: "auto",
  },
  tooltip: {
    maxWidth: `50vw`,
  },
}));

const SalsaInfo = ({ serviceData, jobs }) => {
  const [running, setRunning] = React.useState(0);
  const classes = useStyle();

  React.useEffect(() => {
    setRunning(jobs.reduce((sum, job) => sum + job["R"], 0));
  }, [jobs]);

  if (!serviceData.node) {
    return (
      <Alert severity="error">Info is missing ({serviceData.version})</Alert>
    );
  }

  return (
    <Paper className={classes.wrapper}>
      <Tooltip
        title={<SalsaHosts hosts={serviceData.node.hosts} />}
        classes={{ tooltip: classes.tooltip }}
      >
        <div>
          <p>
            {`Version: ${serviceData.version} Slots: ${
              serviceData.node ? serviceData.node.slots : "N/A"
            } Manager URL: ${serviceData.node && serviceData.node.submitUrl}`}
          </p>
          <LinearProgress
            variant="determinate"
            value={
              serviceData.node
                ? Math.round((running / serviceData.node.slots) * 100)
                : 0
            }
          />
        </div>
      </Tooltip>
    </Paper>
  );
};

export default SalsaInfo;
