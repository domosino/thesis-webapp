import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { v4 } from "uuid";

const useStyle = makeStyles({
  container: {
    fontSize: "16px",
    padding: "1rem",
  },
  value: {
    marginBottom: "1em",
    "&:last-child": {
      marginBottom: 0,
    },
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  adapter: {
    color: "#fff",
    fontWeight: "600",
    marginRight: `1rem`,
  },
});

const SalsaHosts = ({ hosts }) => {
  const classes = useStyle();

  if (!hosts) {
    return (
      <div>
        <span>No workers</span>
      </div>
    );
  }

  return (
    <div className={classes.container}>
      {hosts
        .sort((a, b) => a.hostname.localeCompare(b.hostname))
        .map((host) => {
          return (
            <div key={v4()} className={classes.value}>
              <span className={classes.adapter}>
                {host.hostname ? host.hostname : "N/A"}:
              </span>
              <span>{host.slots}</span>
            </div>
          );
        })}
    </div>
  );
};

export default SalsaHosts;
