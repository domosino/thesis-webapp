import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import ProfileCard from "../molecules/ProfileCard";
import { getAuthenticatedToken, getUserData } from "../selectors/auth";

const ProfilePage = () => {
  const { profile } = useSelector(getUserData);
  const isAuthenticated = useSelector(getAuthenticatedToken);
  const history = useHistory();

  useEffect(() => {
    if (!isAuthenticated) {
      history.push("/auth");
    }
  });

  return (
    <React.Fragment>
      <ProfileCard profile={profile} />
    </React.Fragment>
  );
};

export default ProfilePage;
