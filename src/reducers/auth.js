import {AUTH_USER, AUTH_ERROR, LOADING_LOGIN, SET_USER_DATA} from "../actionTypes/auth";

const authDefaultState = {
    authenticated: "",
    errorMessage: "",
    loadingLogin: false,
    userData: null
};

export default (state = authDefaultState, action) => {
    switch (action.type) {
        case AUTH_USER:
            return {...state, authenticated: action.payload, loadingLogin: false};
        case AUTH_ERROR:
            return {...state, errorMessage: action.payload, loadingLogin: false};
        case LOADING_LOGIN:
            return {...state, loadingLogin: true};
        case SET_USER_DATA:
            return {...state, userData: action.userData};
        default:
            return state;
    }
};
