import { Drawer, List, makeStyles } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import Item from "../atoms/Item";
import { getAppInfo } from "../selectors/websocket";

const getSidebarItems = (services) => {
  const items = [
    {
      name: "Home",
      icon: "home",
      url: "/",
      ifAuth: false,
    },
    {
      name: "ROOT Browser",
      icon: "web",
      url: "/root",
      ifAuth: false,
    },
  ];

  if (services.length === 0) {
    return items;
  }

  const uniqueServices = [...new Set(services.map((service) => service.src))];
  const dynamicItems = uniqueServices.map((service) => {
    if (service === "obmon") {
      return {
        name: "Obmon",
        icon: "table_chart",
        url: "/obmon",
        ifAuth: false,
      };
    } else if (service === "salsa") {
      return {
        name: "Salsa",
        icon: "table_chart",
        url: "/salsa",
        ifAuth: false,
      };
    }
    return {};
  });
  return [...items, ...dynamicItems];
};

const useStyle = makeStyles({
  list: {
    width: 250,
  },
});

const renderItems = (services) => {
  return getSidebarItems(services).map((item, index) => (
    <Item key={index} name={item.name} icon={item.icon} url={item.url} />
  ));
};

const Sidebar = ({ open, toggle }) => {
  const classes = useStyle();
  const { services } = useSelector(getAppInfo);

  return (
    <Drawer open={open} onClose={() => toggle(false)}>
      <div
        tabIndex={0}
        role="button"
        onClick={() => toggle(false)}
        onKeyDown={() => toggle(false)}
      >
        <div className={classes.list}>
          <List>{renderItems(services)}</List>
        </div>
      </div>
    </Drawer>
  );
};

export default Sidebar;
