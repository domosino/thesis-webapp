export const getAuthErrorMessage = (state) => state.auth.errorMessage;
export const getAuthenticatedToken = (state) => state.auth.authenticated;
export const getLoadingLogin = (state) => state.auth.loadingLogin;
export const getUserData = (state) => state.auth.userData;
