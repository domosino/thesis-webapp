const generateTasksData = (jobs) => {
  const now = new Date().getTime() / 1000;
  return jobs
    .sort((a, b) => b.time.started - a.time.started)
    .map((task) => {
      const total = task["A"] + task["P"] + task["R"] + task["D"] + task["F"];
      const percentage = Math.floor(
        ((task["D"] + task["F"]) /
          (task["A"] + task["P"] + task["R"] + task["D"] + task["F"])) *
          100
      );
      const started_time = new Date(task.time.started * 1000);
      const running_time = task.time.finished
        ? task.time.finished - task.time.started
        : Math.round(now) - task.time.started;
      const estimated_time =
        task["P"] + task["R"] > 0 && task["D"] + task["F"] > 0
          ? Math.round(
              ((Math.round(now) - task.time.started) *
                (task["P"] + task["R"])) /
                (task["D"] + task["F"])
            )
          : 0;
      const finished_time = task.time.finished
        ? new Date(task.time.finished * 1000)
        : new Date((task.time.started + running_time + estimated_time) * 1000);
      return {
        name: task.name,
        uid: task.uid,
        gid: task.gid,
        pending: task["A"] + task["P"],
        running: task["R"],
        finished: task["D"],
        failed: task["F"],
        total,
        percentage,
        started_time,
        finished_time,
        running_time,
        estimated_time,
        taskStopped: false,
        rc: task.rc,
      };
    });
};
export default generateTasksData;
