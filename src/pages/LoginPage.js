import {
  Button,
  CircularProgress,
  makeStyles,
  Paper,
  TextField,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import jwtDecode from "jwt-decode";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { authError, loadingLogin as authLogin } from "../actions/auth";
import { AUTH_USER, SET_USER_DATA } from "../actionTypes/auth";
import {
  getAuthenticatedToken,
  getAuthErrorMessage,
  getLoadingLogin,
} from "../selectors/auth";
import { getWebsocketId } from "../selectors/websocket";

const useStyle = makeStyles({
  wrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "calc(100vh - 64px)",
  },
  paper: {
    width: "25rem",
  },
  paperHeader: {
    textAlign: "center",
    backgroundColor: "#3f51b5",
    padding: "1rem",
    color: "white",
  },
  form: {
    padding: "1rem",
  },
  errorMessage: {
    color: "red",
  },
  centered: {
    textAlign: "center",
  },
  progress: {
    margin: "32px",
  },
  button: {
    textAlign: "center",
    margin: "32px 0",
  },
});

const LoginPage = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const wsid = useSelector(getWebsocketId);
  const loadingLogin = useSelector(getLoadingLogin);
  const authErrorMessage = useSelector(getAuthErrorMessage);
  const isAuthenticated = useSelector(getAuthenticatedToken);
  const classes = useStyle();
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    if (isAuthenticated) {
      history.push("/");
    }
  });

  const submit = async () => {
    dispatch(authError());
    dispatch(authLogin());
    const userData = { username, password, wsid };
    try {
      const POSTtoURL = `${window.env.HTTP_PROTOCOL}//${window.env.API_URL}/login`;
      const response = await axios({
        method: "post",
        url: POSTtoURL,
        timeout: window.env.HTTP_REQUEST_TIMEOUT,
        data: userData,
      });

      dispatch({
        type: AUTH_USER,
        payload: response.data.token,
      });

      localStorage.setItem("token", response.data.token);

      const decodedToken = jwtDecode(response.data.token);

      dispatch({
        type: SET_USER_DATA,
        userData: decodedToken,
      });

      history.push("/profile");
    } catch (error) {
      if (error.response) {
        dispatch(authError(error.response.data));
      } else {
        dispatch(authError("Server did not respond. Try again"));
      }
    }
  };

  return (
    <div className={classes.wrapper}>
      <Paper className={classes.paper} elevation={1}>
        <Typography variant="h5" className={classes.paperHeader}>
          Login
        </Typography>
        <form noValidate autoComplete="off" className={classes.form}>
          <TextField
            label="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            margin="normal"
            type="text"
            fullWidth
            autoFocus
          />
          <TextField
            label="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            margin="normal"
            type="password"
            fullWidth
          />
          {authErrorMessage && (
            <div className={classes.errorMessage}>{authErrorMessage}</div>
          )}
          {loadingLogin ? (
            <div className={classes.centered}>
              <CircularProgress className={classes.progress} size={50} />
            </div>
          ) : (
            <div className={classes.button}>
              <Button
                onClick={submit}
                variant="contained"
                color="primary"
                type="submit"
                disabled={username.length === 0 || password.length === 0}
              >
                Login
              </Button>
            </div>
          )}
        </form>
      </Paper>
    </div>
  );
};

export default LoginPage;
