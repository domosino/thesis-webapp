import { TableRow } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import TableCell from "./TableCell";
import { getServiceData } from "../selectors/zmq2wsServiceInfo";

let secondaryTableHeaders = [
  "id",
  "name",
  "cores",
  "load",
  "sys",
  "user",
  "nice",
  "iowait",
  "idle",
  "used",
  "cached",
  "total",
  "Write",
  "Read",
  "In",
  "Out",
];

const TableContent = () => {
  const tableData = useSelector(getServiceData);
  const renderSubHeaders = () => {
    let nodeWithGPU = false;
    tableData.forEach((node) => {
      if (node.gpu) {
        nodeWithGPU = true;
      }
    });
    if (nodeWithGPU) {
      return [
        ...secondaryTableHeaders,
        "GPU's",
        "Usage",
        "Used mem.",
        "Total mem.",
      ];
    }
    return secondaryTableHeaders;
  };
  const subHeaders = renderSubHeaders();
  return (
    <TableRow>
      {subHeaders.map((header, index) => (
        <TableCell key={index}>{header}</TableCell>
      ))}
    </TableRow>
  );
};

export default TableContent;
