import { ActionTypes } from "@obl/redux-websocket-middleware";
import { RECEIVE_APP_INFO_SUCCESS } from "../actionTypes/websocket";
import {
  GET_ZMQ2SERVICE_NAME,
  GET_ZMQ2SERVICE_SUB,
  GET_ZMQ2SERVICE_SRC,
  GET_ZMQ2SERVICE_DATA,
  GET_ZMQ2SERVICE_ISSUBSCRIBED,
} from "../actionTypes/zmq2wsServiceInfo";

const zmq2wsServiceInfoDefaultState = {
  name: "",
  sub: "",
  src: "",
  data: [],
  isSub: false,
};

export default (state = zmq2wsServiceInfoDefaultState, action) => {
  switch (action.type) {
    case ActionTypes.RECEIVED_WEBSOCKET_DATA: {
      const { type } = action.payload;
      if (type === "zmq") {
        if (
          action.payload.data.name === state.name &&
          action.payload.data.sub === state.sub
        ) {
          if (action.payload.data.data.length !== 0) {
            return {
              ...state,
              name: action.payload.data.name,
              sub: action.payload.data.sub,
              data: action.payload.data.data,
              src: action.payload.src,
            };
          } else {
            return zmq2wsServiceInfoDefaultState;
          }
        }
      }
      return state;
    }
    case GET_ZMQ2SERVICE_NAME:
      return { ...state, name: action.name };

    case GET_ZMQ2SERVICE_SUB:
      return { ...state, sub: action.sub, isSub: false };

    case GET_ZMQ2SERVICE_SRC:
      return { ...state, src: action.src };

    case GET_ZMQ2SERVICE_DATA:
      return { ...state, data: action.data };

    case GET_ZMQ2SERVICE_ISSUBSCRIBED:
      return { ...state, isSub: action.isSub };

    case RECEIVE_APP_INFO_SUCCESS:
      return state;
    default:
      return state;
  }
};
