import { Card, CardContent, makeStyles, Typography } from "@material-ui/core";
import React from "react";
import Gravatar from "react-gravatar";

const useStyle = makeStyles({
  wrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "calc(100vh - 64px)",
  },
  card: {
    maxWidth: 300,
  },
  placeholder: {
    fontSize: "16px",
    fontWeight: 800,
  },
  header: {
    textAlign: "center",
    backgroundColor: "#3f51b5",
    color: "#fff",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "1rem",
  },
  gravatar: {
    width: "80px",
    height: "80px",
    borderRadius: "50%",
  },
});

const ProfileCard = ({ profile }) => {
  const classes = useStyle();
  return (
    <div className={classes.wrapper}>
      <Card className={classes.card}>
        <Typography variant="h5" className={classes.header}>
          <p>Profile</p>
          <Gravatar
            email={profile.mail}
            className={classes.gravatar}
            default="monsterid"
          />
        </Typography>
        <CardContent>
          <Typography variant="subtitle1">
            <span className={classes.placeholder}>Id:</span> {profile.uid}
          </Typography>
          <Typography variant="subtitle1">
            <span className={classes.placeholder}>Name:</span>{" "}
            {profile.displayName}
          </Typography>
          <Typography variant="subtitle1">
            <span className={classes.placeholder}>Email:</span> {profile.mail}
          </Typography>
          <Typography variant="subtitle1">
            <span className={classes.placeholder}>Home directory:</span>{" "}
            {profile.homeDirectory}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
};

export default ProfileCard;
