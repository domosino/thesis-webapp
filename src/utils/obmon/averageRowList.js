import {
  calculatedCoresTotalAndAverage,
  generateTotalNumberLoad1,
  generateCpuAverage,
  generateMemoryAverageAndTotal,
  generateDisksAverageAndTotal,
  generateNetworkAverageAndTotal
} from "../utils";
import { v4 } from "uuid";

const averageRowList = (tableRowsData, classes, findInsideTableRows) => [
  { id: v4(), className: classes.total, colSpan: 2, children: `Average` },
  {
    id: v4(),
    children: calculatedCoresTotalAndAverage(tableRowsData, "cores")
  },
  { id: v4(), children: generateTotalNumberLoad1(tableRowsData, "load1") },
  { id: v4(), children: generateTotalNumberLoad1(tableRowsData, "load1") },
  {
    id: v4(),
    className: classes.widthCell,
    children: `${generateCpuAverage(tableRowsData, "sys")} %`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `${generateCpuAverage(tableRowsData, "user")} %`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `${generateCpuAverage(tableRowsData, "iowait")} %`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: `${generateCpuAverage(tableRowsData, "idle")} %`
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateMemoryAverageAndTotal(tableRowsData, "used")
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateMemoryAverageAndTotal(tableRowsData, "cached")
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateMemoryAverageAndTotal(tableRowsData, "totalMEMORY")
  },
  {
    id: v4(),
    children: generateDisksAverageAndTotal(tableRowsData, "disk_write")
  },
  {
    id: v4(),
    children: generateDisksAverageAndTotal(tableRowsData, "disk_read")
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateNetworkAverageAndTotal(tableRowsData, "bytesIn")
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: generateNetworkAverageAndTotal(tableRowsData, "bytesOut")
  },
  ...(findInsideTableRows
    ? ["", "", "", ""].map(x => ({ id: v4(), children: `-` }))
    : [])
];

export default averageRowList;
