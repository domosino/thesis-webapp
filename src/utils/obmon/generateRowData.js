import {
  calculatedUsedValue,
  bytesIn,
  bytesOut,
  bytesInDisk,
  bytesOutDisk,
  GpuLoad,
  GpuUsedMemory,
  GpuTotalMemory
} from "../utils";
import { v4 } from "uuid";

const generateRowData = (data, tableData, id) => {
  let nodeWithGPU = false;
  tableData.forEach(node => {
    if (node.gpu) {
      nodeWithGPU = true;
    }
  });
  if (nodeWithGPU) {
    return {
      id,
      uuid: v4(),
      name: data.nodename,
      cores: data.sys.value.cpus,
      load1: data.sys.value.load1.value,
      sys: data.sys.change.cpu.sys,
      user: data.sys.change.cpu.user,
      nice: data.sys.change.cpu.nice,
      iowait: data.sys.change.cpu.iowait,
      idle: data.sys.change.cpu.idle,
      used: calculatedUsedValue(data.sys.value.mem),
      cached: data.sys.value.mem.cached,
      totalMEMORY: data.sys.value.mem.total,
      bytesIn: bytesIn(data),
      bytesOut: bytesOut(data),
      disk_read: bytesInDisk(data),
      disk_write: bytesOutDisk(data),
      numberOfGpus: data.gpu && data.gpu.value.gpus.length,
      loadGpu: data.gpu && GpuLoad(data),
      usedMemoryGpu: data.gpu && GpuUsedMemory(data),
      totalMemoryGpu: data.gpu && GpuTotalMemory(data)
    };
  } else {
    return {
      id,
      uuid: v4(),
      name: data.nodename,
      cores: data.sys.value.cpus,
      load1: data.sys.value.load1.value,
      sys: data.sys.change.cpu.sys,
      user: data.sys.change.cpu.user,
      nice: data.sys.change.cpu.nice,
      iowait: data.sys.change.cpu.iowait,
      idle: data.sys.change.cpu.idle,
      used: calculatedUsedValue(data.sys.value.mem),
      cached: data.sys.value.mem.cached,
      totalMEMORY: data.sys.value.mem.total,
      bytesIn: bytesIn(data),
      bytesOut: bytesOut(data),
      disk_read: bytesInDisk(data),
      disk_write: bytesOutDisk(data)
    };
  }
};

export default generateRowData;
