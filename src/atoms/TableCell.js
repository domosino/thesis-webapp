import { TableCell, withStyles } from "@material-ui/core";

export default withStyles((theme) => {
  return {
    head: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 13,
      paddingRight: "15px",
    },
  };
})(TableCell);
