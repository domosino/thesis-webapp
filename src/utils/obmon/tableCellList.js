import React from "react";
import {
  generateBackgroundColor,
  decimalPlace,
  bytesToSize,
  readableByteInSeconds,
  bytesToSizeInSeconds,
} from "../utils";
import { Tooltip } from "@material-ui/core";
import DiskWrite from "../../molecules/DiskWrite";
import DiskRead from "../../molecules/DiskRead";
import BytesInAdapter from "../../molecules/BytesInAdapter";
import BytesOutAdapter from "../../molecules/BytesOutAdapter";
import { v4 } from "uuid";

const tableCellList = (row, classes) => [
  {
    id: v4(),
    className: classes.idCell,
    children: row.id,
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: row.name,
  },
  {
    id: v4(),
    className: classes.widthCell,
    children: row.cores,
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { backgroundColor: generateBackgroundColor(row.cores, row.load1) },
    children: decimalPlace(row.load1, 2),
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { backgroundColor: `rgba(211, 47, 47, ${row.sys.alpha})` },
    children: `${decimalPlace(row.sys.value, 1)} %`,
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { backgroundColor: `rgba(244, 67, 54, ${row.user.alpha})` },
    children: `${decimalPlace(row.user.value, 1)} %`,
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { backgroundColor: `rgba(255, 235, 59, ${row.nice.alpha})` },
    children: `${decimalPlace(row.nice.value, 1)} %`,
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { backgroundColor: `rgba(255, 235, 59, ${row.iowait.alpha})` },
    children: `${decimalPlace(row.iowait.value, 1)} %`,
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { backgroundColor: `rgba(205, 220, 57, ${row.idle.alpha})` },
    children: `${decimalPlace(row.idle.value, 1)} %`,
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { backgroundColor: `rgba(244, 67, 54, ${row.used.alpha})` },
    children: bytesToSize(row.used.value),
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { backgroundColor: `rgba(255, 238, 88, ${row.cached.alpha})` },
    children: bytesToSize(row.cached.value),
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { backgroundColor: `#fff` },
    children: bytesToSize(row.totalMEMORY.value),
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: {
      backgroundColor:
        row.disk_write && `rgba(66,165,245, ${row.disk_write.alpha})`,
      cursor: "help",
    },
    children: (
      <Tooltip
        title={<DiskWrite disks={row.disk_write && row.disk_write.disks} />}
        placement="top"
      >
        <div>
          {readableByteInSeconds(row.disk_write && row.disk_write.value)}
        </div>
      </Tooltip>
    ),
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: {
      backgroundColor:
        row.disk_read && `rgba(66,165,245, ${row.disk_read.alpha})`,
      cursor: "help",
    },
    children: (
      <Tooltip
        title={<DiskRead disks={row.disk_read && row.disk_read.disks} />}
        placement="top"
      >
        <div>{readableByteInSeconds(row.disk_read && row.disk_read.value)}</div>
      </Tooltip>
    ),
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: {
      backgroundColor: `rgba(66,165,245, ${row.bytesIn.alpha})`,
      cursor: "help",
    },
    children: (
      <Tooltip title={<BytesInAdapter adapter={row} />} placement="top">
        <div>{bytesToSizeInSeconds(row.bytesIn.value)}</div>
      </Tooltip>
    ),
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: {
      backgroundColor: `rgba(179,136,255, ${row.bytesOut.alpha})`,
      cursor: "help",
    },
    children: (
      <Tooltip title={<BytesOutAdapter adapter={row} />} placement="top">
        <div>{bytesToSizeInSeconds(row.bytesOut.value)}</div>
      </Tooltip>
    ),
  },
];

export default tableCellList;
