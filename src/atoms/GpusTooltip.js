import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { decimalPlace } from "../utils/utils";

const useStyle = makeStyles({
  container: {
    fontSize: "16px",
    padding: "5px",
  },
  value: {
    marginBottom: "16px",
    "&:last-child": {
      marginBottom: 0,
    },
  },
  adapter: {
    color: "#fff",
    fontWeight: "600",
  },
});

const GpusTooltip = ({ gpus }) => {
  const classes = useStyle();
  return (
    <div className={classes.container}>
      {gpus &&
        gpus.map((node, index) => {
          return (
            <div key={index} className={classes.value}>
              <span className={classes.adapter}>{node.name}</span>:{" "}
              {decimalPlace(node.load, 1) + "%"}
            </div>
          );
        })}
    </div>
  );
};

export default GpusTooltip;
