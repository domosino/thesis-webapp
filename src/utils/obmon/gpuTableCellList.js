import React from "react";
import { decimalPlace, bytesToSize } from "../utils";
import { Tooltip } from "@material-ui/core";
import GpusTooltip from "../../atoms/GpusTooltip";
import GpusUsedTooltip from "../../atoms/GpusUsedTooltip";
import GpusTotalTooltip from "../../atoms/GpusTotalTooltip";
import { v4 } from "uuid";

const gpuTableCellList = (row, classes) => [
  {
    id: v4(),
    className: classes.widthCell,
    children: row.numberOfGpus ? row.numberOfGpus : "-",
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: {
      backgroundColor: row.loadGpu
        ? `rgba(116,183,27, ${decimalPlace(
            row.loadGpu.load / row.numberOfGpus,
            1,
            true
          )})`
        : "white",
      cursor: "help",
    },
    children: (
      <Tooltip
        title={<GpusTooltip gpus={row.loadGpu && row.loadGpu.gpus} />}
        placement="top"
      >
        <div>
          {row.loadGpu ? (
            <React.Fragment>
              {decimalPlace(row.loadGpu.load / row.numberOfGpus, 1)} %
            </React.Fragment>
          ) : (
            "-"
          )}
        </div>
      </Tooltip>
    ),
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: {
      backgroundColor: row.usedMemoryGpu
        ? `rgba(244,67,54, ${row.usedMemoryGpu.alpha})`
        : "white",
      cursor: "help",
    },
    children: (
      <Tooltip
        title={
          <GpusUsedTooltip gpus={row.usedMemoryGpu && row.usedMemoryGpu.gpus} />
        }
        placement="top"
      >
        <div>
          {row.loadGpu ? (
            <React.Fragment>
              {row.usedMemoryGpu ? bytesToSize(row.usedMemoryGpu.used) : "-"}
            </React.Fragment>
          ) : (
            "-"
          )}
        </div>
      </Tooltip>
    ),
  },
  {
    id: v4(),
    className: classes.widthCell,
    style: { cursor: "help" },
    children: (
      <Tooltip
        title={
          <GpusTotalTooltip
            gpus={row.totalMemoryGpu && row.usedMemoryGpu.gpus}
          />
        }
        placement="top"
      >
        <div>
          {row.loadGpu ? (
            <React.Fragment>
              {row.totalMemoryGpu ? bytesToSize(row.totalMemoryGpu.total) : "-"}
            </React.Fragment>
          ) : (
            "-"
          )}
        </div>
      </Tooltip>
    ),
  },
];

export default gpuTableCellList;
