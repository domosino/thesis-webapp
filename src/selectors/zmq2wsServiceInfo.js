export const getServiceName = state => state.zmq2wsServiceInfo.name;
export const getServiceSub = state => state.zmq2wsServiceInfo.sub;
export const getServiceSrc = state => state.zmq2wsServiceInfo.src;
export const getServiceData = state => state.zmq2wsServiceInfo.data;
export const getServiceIsSub = state => state.zmq2wsServiceInfo.isSub;
