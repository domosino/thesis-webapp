import { ActionTypes } from "@obl/redux-websocket-middleware";

const websocketDefaultState = {
  services: [],
  clients: [],
  name: "",
  version: "",
  id: "",
  wsConnected: false,
};

export default (state = websocketDefaultState, action) => {
  switch (action.type) {
    case ActionTypes.RECEIVED_WEBSOCKET_DATA: {
      const { type, payload } = action.payload;
      if (type === "ws") {
        const { id } = payload;
        return {
          ...state,
          id,
        };
      }
      if (type === "app") {
        let { name, version, clients, services } = payload;
        return {
          ...state,
          name,
          version,
          clients,
          services,
        };
      }
      return state;
    }
    case ActionTypes.WEBSOCKET_DISCONNECTED:
      return {
        ...state,
        id: "",
        wsConnected: false,
      };
    case ActionTypes.WEBSOCKET_CONNECTED:
      return {
        ...state,
        wsConnected: true,
      };
    case ActionTypes.WEBSOCKET_ERROR:
      console.log(action.payload);
      return state;
    default:
      return state;
  }
};
