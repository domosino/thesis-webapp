import { v4 } from "uuid";
import React from "react";
import pretty from "pretty-time";
import { LinearProgress, Tooltip } from "@material-ui/core";
import ReactTimeAgo from "react-time-ago";
import SalsaTooltip from "../../atoms/SalsaTooltip";

const formatDate = (date) => {
  return `${date.getDate()}.${
    date.getMonth() + 1
  }.${date.getFullYear()} ${date
    .getHours()
    .toString()
    .padStart(2, "0")}:${date
    .getMinutes()
    .toString()
    .padStart(2, "0")}:${date.getSeconds().toString().padStart(2, "0")}`;
};

const taskCellList = (task, classes, tasksData) => {
  return [
    {
      id: v4(),
      className: classes.widthCell,
      children: (
        <React.Fragment>
          <p>{task.name}</p>
          <LinearProgress
            variant="buffer"
            value={Math.floor(
              ((task.finished + task.failed) * 100) / task.total
            )}
            valueBuffer={
              Math.floor(((task.finished + task.failed) * 100) / task.total) +
              Math.floor((task.running * 100) / task.total)
            }
          />
        </React.Fragment>
      ),
    },
    {
      id: v4(),
      className: classes.widthCell,
      children: task.uid,
    },
    {
      id: v4(),
      className: classes.widthCell,
      children: task.gid,
    },
    {
      id: v4(),
      className: classes.widthCell,
      children: task.total,
    },
    {
      id: v4(),
      className: classes.widthCell,
      children: task.pending,
      style: {
        backgroundColor: `rgba(0,102,255,${task.pending / task.total})`,
      },
    },
    {
      id: v4(),
      className: classes.widthCell,
      children: task.running,
      style: {
        backgroundColor: `rgba(255,165,0,${
          task.running / tasksData.reduce((sum, task) => sum + task.running, 0)
        })`,
      },
    },
    {
      id: v4(),
      className: classes.widthCell,
      children:
        task.rc.done.length > 0 ? (
          <Tooltip title={<SalsaTooltip data={task.rc.done} />} placement="top">
            <div>{task.finished}</div>
          </Tooltip>
        ) : (
          task.finished
        ),
      style: {
        backgroundColor: `rgba(0,255,0,${task.finished / task.total})`,
      },
    },
    {
      id: v4(),
      className: classes.widthCell,
      children:
        task.rc.failed.length > 0 ? (
          <Tooltip
            title={<SalsaTooltip data={task.rc.failed} />}
            placement="top"
          >
            <div>{task.failed}</div>
          </Tooltip>
        ) : (
          task.failed
        ),
      style: {
        backgroundColor: `rgba(255,0,0,${task.failed / task.total})`,
      },
    },
    {
      id: v4(),
      className: classes.widthCell,
      children: `${task.percentage} %`,
    },
    {
      id: v4(),
      className: classes.widthCell,
      children: (
        <React.Fragment>
          <Tooltip title={formatDate(task.started_time)}>
            <ReactTimeAgo date={task.started_time} />
          </Tooltip>
        </React.Fragment>
      ),
    },
    {
      id: v4(),
      className: classes.widthCell,
      children: pretty([task.running_time, 1e-9], "s"),
    },
    {
      id: v4(),
      className: classes.widthCell,
      children:
        task.estimated_time === 0
          ? "-"
          : pretty([task.estimated_time, 1e-9], "s"),
    },
    {
      id: v4(),
      className: classes.widthCell,
      children: (
        <React.Fragment>
          <Tooltip title={formatDate(task.finished_time)}>
            <ReactTimeAgo date={task.finished_time} />
          </Tooltip>
        </React.Fragment>
      ),
    },
  ];
};

export default taskCellList;
