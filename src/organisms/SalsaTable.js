import {
  Checkbox,
  FormControlLabel,
  makeStyles,
  Table,
} from "@material-ui/core";
import React, { Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getAuthenticatedToken, getUserData } from "../selectors/auth";
import { getServiceData } from "../selectors/zmq2wsServiceInfo";
import generateTasksData from "../utils/salsa/tasksData";
import SalsaTableContent from "../molecules/SalsaTableContent";
import SalsaTableHeader from "../atoms/SalsaTableHeader";

const useStyle = makeStyles((theme) => ({
  checkbox: {
    display: "flex",
    justifyContent: "flex-start",
  },
  labels: {
    paddingLeft: theme.spacing(2),
    display: "flex",
  },
}));

const SalsaTable = () => {
  const classes = useStyle();
  const { jobs } = useSelector(getServiceData);
  const auth = useSelector(getAuthenticatedToken);
  const userData = useSelector(getUserData);
  const [isChecked, setIsChecked] = useState(auth ? true : false);
  const dense = localStorage.getItem("isDenseTable");
  const [isDense, setIsDense] = useState(dense === "true" ? true : false);

  useEffect(() => {
    localStorage.setItem("isDenseTable", isDense);
  }, [isDense]);

  if (jobs.length === 0) {
    return null;
  }

  let tasksData = generateTasksData(jobs);
  if (isChecked && auth && userData && userData.profile) {
    const uid = parseInt(userData.profile.uidNumber);
    tasksData = tasksData.filter((task) => task.uid === uid);
  }

  return (
    <Fragment>
      <div className={classes.labels}>
        {auth && (
          <FormControlLabel
            className={classes.checkbox}
            control={
              <Checkbox
                checked={isChecked}
                onChange={(e) => setIsChecked(e.target.checked)}
                color="primary"
              />
            }
            label="Show only my jobs"
          />
        )}
        <FormControlLabel
          control={
            <Checkbox
              checked={isDense}
              onChange={(e) => setIsDense(e.target.checked)}
              color="primary"
            />
          }
          color="primary"
          label="Show dense table"
        />
      </div>
      {tasksData.length !== 0 ? (
        <Table size={isDense ? "small" : "medium"}>
          <SalsaTableHeader />
          <SalsaTableContent tasksData={tasksData} />
        </Table>
      ) : (
        <div style={{ textAlign: "center" }}>
          <h2>No jobs in queue</h2>
        </div>
      )}
    </Fragment>
  );
};

export default SalsaTable;
