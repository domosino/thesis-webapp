import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  makeStyles,
  Paper,
  Tooltip,
  Typography,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PersonIcon from "@material-ui/icons/Person";
import Alert from "@material-ui/lab/Alert";
import GoogleMapReact from "google-map-react";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import PieChart from "../molecules/PieChart";
import UsersTable from "../molecules/UsersTable";
import { getAppInfo } from "../selectors/websocket";

const useStyle = makeStyles((theme) => ({
  root: {
    width: "98vw",
    margin: "16px auto",
    textAlign: "center",
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
  },

  maps: {
    height: "70vh",
    flex: 1,
    marginRight: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      marginBottom: theme.spacing(3),

      "& > div": {
        width: "100% !important",
        height: "inherit !important",
      },
    },
  },

  paper: {
    flex: 1,
    overflow: "hidden",
    margin: "auto",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },

  expanded: {
    flex: 1,
    overflow: "hidden",
    margin: "auto !important",
    // "&:nth-child(1)": {
    //   minHeight: "calc(70vh - 96px)",
    // },
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },

  table: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  },
  chart: {
    flex: 1,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    margin: "-12px 0 0 -12px",
  },
  alert: {
    flex: 1,
    borderRadius: "10px",
    marginTop: theme.spacing(2),
  },
  mapErrorWrapper: {
    display: "flex",
    flexDirection: "column",
  },
}));

const ApiInfoPage = () => {
  const { clients, name, version } = useSelector(getAppInfo);
  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    handleChange("info")(null, true);
  }, []);

  const classes = useStyle();

  if (clients.length === 0) {
    return <h1>Not showing table</h1>;
  }

  const clientsData = clients
    .sort((a, b) => a.username.localeCompare(b.username))
    .map((client, index) => {
      if (!client.geo) {
        return {
          index: index + 1,
          id: client.id,
          browser: client.info.family,
          os: client.info.os.family,
          username: client.username !== "" ? client.username : "anonymous",
        };
      }
      return {
        index: index + 1,
        id: client.id,
        country: client.geo.country,
        lat: client.geo.ll[0],
        lng: client.geo.ll[1],
        browser: client.info.family,
        os: client.info.os.family,
        username: client.username !== "" ? client.username : "anonymous",
      };
    });

  return (
    <div className={classes.root}>
      <Paper className={classes.maps}>
        {window.env.REACT_APP_MAPS_KEY ? (
          <GoogleMapReact
            bootstrapURLKeys={{
              key: window.env.REACT_APP_MAPS_KEY,
              language: "en",
              region: "en",
            }}
            defaultCenter={{ lat: 48.7152163, lng: 16.2587324 }}
            defaultZoom={4}
          >
            {clientsData
              .filter((client) => client.country)
              .map((client) => (
                <div key={client.id} lat={client.lat} lng={client.lng}>
                  <Tooltip title={client.username}>
                    <PersonIcon className={classes.icon} />
                  </Tooltip>
                </div>
              ))}
          </GoogleMapReact>
        ) : (
          <div className={classes.mapErrorWrapper}>
            <Alert className={classes.alert} severity="warning">
              Google Maps API key is missing. Please set it in
              /etc/slswebapp/env.js as REACT_APP_MAPS_KEY
            </Alert>
            <h1>Google Map needs some configuration !!!</h1>
          </div>
        )}
      </Paper>
      <div>
        <ExpansionPanel
          className={classes.paper}
          expanded={expanded === "info"}
          onChange={handleChange("info")}
          classes={{ expanded: classes.expanded }}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>OBMON services</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.chart}>
            <PieChart />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel
          className={classes.paper}
          expanded={expanded === "users"}
          onChange={handleChange("users")}
          classes={{ expanded: classes.expanded }}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Users</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.table}>
            <UsersTable clientsData={clientsData} />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel
          className={classes.paper}
          expanded={expanded === "appinfo"}
          onChange={handleChange("appinfo")}
          classes={{ expanded: classes.expanded }}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Info</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.table}>
            <Typography variant="inherit" color="inherit">
              App: {window.webapp.name} (v{window.webapp.version})
            </Typography>
            <Typography variant="inherit" color="inherit">
              API: {name} (v{version})
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    </div>
  );
};

export default ApiInfoPage;
