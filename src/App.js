import CssBaseline from "@material-ui/core/CssBaseline";
import React from "react";
import { Route, Switch } from "react-router-dom";
import Navbar from "./molecules/Navbar";
import ApiInfoPage from "./pages/ApiInfoPage";
import LoginPage from "./pages/LoginPage";
import ObmonTablePage from "./pages/ObmonTablePage";
import ProfilePage from "./pages/ProfilePage";
import RootBrowserPage from "./pages/RootBrowserPage";
import SalsaTablePage from "./pages/SalsaTablePage";

const App = () => {
  return (
    <React.Fragment>
      <CssBaseline />
      <Navbar />
      <Switch>
        <Route exact path="/profile">
          <ProfilePage />
        </Route>
        <Route exact path="/auth">
          <LoginPage />
        </Route>
        <Route path="/obmon/:service?/:sub?">
          <ObmonTablePage />
        </Route>
        <Route path="/salsa/:service?/:sub?">
          <SalsaTablePage />
        </Route>
        <Route path="/api/info">
          <ApiInfoPage />
        </Route>
        <Route path="/root">
          <RootBrowserPage />
        </Route>
        <Route path="/">
          <ApiInfoPage />
        </Route>
      </Switch>
    </React.Fragment>
  );
};
export default App;
